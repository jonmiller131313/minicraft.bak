cmake_minimum_required(VERSION 3.10)

option(DISABLE_DOCS "Disable documentation generation with 'docs' target." NO)
if(${DISABLE_DOCS})
    return()
endif()

find_package(Doxygen)

if(${DOXYGEN_FOUND})
    set(DOXYGEN_OUTPUT_DIRECTORY "docs")
    set(DOXYGEN_JAVADOC_AUTOBRIEF "YES")
    set(DOXYGEN_EXTRACT_PRIVATE "YES")
    set(DOXYGEN_EXTRACT_STATIC "YES")
    set(DOXYGEN_SORT_MEMGERS_CTORS_1ST "YES")
    set(DOXYGEN_WARN_IF_UNDOCUMENTED "NO")
    set(DOXYGEN_GENERATE_TREEVIEW "YES")
    set(DOXYGEN_SOURCE_BROWSER "YES")
    set(DOXYGEN_BUILTIN_STL_SUPPORT "YES")
    set(DOXYGEN_HTML_DYNAMIC_SECTIONS "YES")
    set(DOXYGEN_HTML_TIMESTAMP "YES")
    set(DOXYGEN_QUIET "YES")
    set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/docs")

    doxygen_add_docs(docs
                     "${SOURCE_DIR}" "README.md" "LICENSE" "CHANGELOG.md"
                     COMMENT "Generating ${GAME_TARGET} doxygen documentation.")
else()
    message(WARNING "Project will build, but will be unable to generate documentation, suppress with -DDISABLE_DOCS=YES")
endif()
