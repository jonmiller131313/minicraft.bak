# Minicraft
[![Version](https://img.shields.io/badge/Version-0.0.1-informational.svg)](CHANGELOG.md)

A simple clone of Notch's [Minicraft game](https://en.wikipedia.org/wiki/Minicraft).

## Getting Started
### Dependencies:
 - C++17
 - CMake 3.10
 - [Allegro](https://liballeg.org/)
 - [XDGDirs](https://gitlab.com/jonmiller131313/XDGDirs)
 - [SimpleCppLogger](https://gitlab.com/jonmiller131313/SimpleCppLogger)
 - [Doxygen](https://doxygen.nl/) _(optional)_

 ### Building/Installing
Clone the repo, then run:
```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j
```

#### CMake options:
 - `-DDISABLE_DOCS=YES`: Disable generating documentation with `docs` target
   - Documentation generation is enabled by default, use this option to suppress missing Doxygen dependency warning.
