cmake_minimum_required(VERSION 3.10)

#add_subdirectory()

set(SOURCES
    EventGenerator.cpp
    keyboard_handling.cpp
    Timer.cpp
)

foreach(file ${SOURCES})
    list(APPEND LOCAL_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/${file}")
endforeach()

set(GAME_SOURCES
    ${GAME_SOURCES} ${LOCAL_SOURCES}
    PARENT_SCOPE
)

