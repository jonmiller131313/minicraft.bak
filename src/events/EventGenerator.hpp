/**
 * @file EventGenerator.hpp
 * @brief
 */
#ifndef MINICRAFT_EVENTGENERATOR_HPP
#define MINICRAFT_EVENTGENERATOR_HPP

#include <allegro5/allegro5.h>

namespace minicraft {

class EventQueue;

class EventGenerator {
    public:
        virtual ~EventGenerator() = default;

    private:
        virtual ALLEGRO_EVENT_SOURCE *getEventSource() const = 0;

        friend EventQueue;
};

};

#endif
