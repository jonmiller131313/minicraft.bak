#include "keyboard_handling.hpp"

namespace minicraft {

static const uint8_t KEY_UNSEEN = 1;
static const uint8_t KEY_PRESSED = 2;

static uint8_t keys[ALLEGRO_KEY_MAX] = {0};
static std::vector<KeyboardCallback> oneshotCallbacks;
static std::vector<KeyboardCallback> pressCallbacks;

static void keyboardEventCallback(ALLEGRO_EVENT *event) {
    switch(event->type) {
        case ALLEGRO_EVENT_KEY_UP:
            keys[event->keyboard.keycode] &= ~KEY_PRESSED;
            return;

        case ALLEGRO_EVENT_KEY_DOWN: {
            uint8_t keycode = event->keyboard.keycode;
            keys[keycode] = KEY_PRESSED | KEY_UNSEEN;
            for(const auto &fn : oneshotCallbacks)
                fn(keycode);
            break;
        }

        case ALLEGRO_EVENT_TIMER:
            for(uint8_t key = 0; key < ALLEGRO_KEY_MAX; key++) {
                if(!keys[key])
                    continue;
                for(const auto &fn : pressCallbacks)
                    fn(key);
                keys[key] &= ~KEY_UNSEEN;
            }
            break;
    }
}

void registerKeyboardEvents(EventQueue &queue) {
    al_register_event_source(queue.queue, al_get_keyboard_event_source());
    queue.registerCallback(ALLEGRO_EVENT_KEY_DOWN, keyboardEventCallback);
    queue.registerCallback(ALLEGRO_EVENT_KEY_UP, keyboardEventCallback);
    queue.registerCallback(ALLEGRO_EVENT_TIMER, keyboardEventCallback);
}

void registerKeyboardCallback(const KeyboardCallback &callback, KeyboardCallbackScope scope) {
    switch(scope) {
        case PRESS:
            pressCallbacks.push_back(callback);
            break;

        case ONESHOT:
            oneshotCallbacks.push_back(callback);
            break;

        default:
            assert(false);
    }
}

void clearKeyboardCallbacks() {
    oneshotCallbacks.clear();
    pressCallbacks.clear();
    memset(keys, 0, ALLEGRO_KEY_MAX);
}

};
