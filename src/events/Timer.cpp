#include "Timer.hpp"

using namespace minicraft;

Timer::Timer() : timer(nullptr) {

}

Timer::~Timer() {
    if(timer && al_is_system_installed())
        al_destroy_timer(timer);
}

bool Timer::createTimer(double interval) {
    if(timer)
        return true;

    timer = al_create_timer(interval);
    return timer != nullptr;
}

ALLEGRO_EVENT_SOURCE *Timer::getEventSource() const {
    assert(timer != nullptr);
    return al_get_timer_event_source(timer);
}

void Timer::start() {
    assert(timer != nullptr);
    al_start_timer(timer);
}
