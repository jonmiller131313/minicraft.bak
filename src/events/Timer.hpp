/**
 * @file Timer.hpp
 * @brief
 */
#ifndef MINICRAFT_TIMER_HPP
#define MINICRAFT_TIMER_HPP

#include <allegro5/allegro5.h>

#include "EventGenerator.hpp"

namespace minicraft {

class Timer : public EventGenerator {
    public:
        Timer();

        ~Timer();

        bool createTimer(double interval);

        ALLEGRO_EVENT_SOURCE *getEventSource() const override;

        void start();

    private:
        ALLEGRO_TIMER *timer;
};

};

#endif
