/**
 * @file keyboard_handling.hpp
 * @brief
 */
#ifndef MINICRAFT_KEYBOARD_HANDLING_HPP
#define MINICRAFT_KEYBOARD_HANDLING_HPP

#include <algorithm>
#include <functional>
#include <vector>

#include <allegro5/allegro5.h>

#include "EventQueue.hpp"

namespace minicraft {

typedef enum {
    ONESHOT,
    PRESS
} KeyboardCallbackScope;

using KeyboardCallback = std::function<void(int)>;

void registerKeyboardCallback(const KeyboardCallback &callback, KeyboardCallbackScope scope = PRESS);

void clearKeyboardCallbacks();

};

#endif
