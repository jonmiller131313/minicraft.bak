#include "Config.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

#if !NDEBUG
const fs::path Config::DATA_DIR = fs::current_path() / "rsc";
#else
const fs::path Config::DATA_DIR = fs::path(xdg_dirs_data_home()) / "Minicraft";
#endif

static bool checkedDataDir = false;

const std::map<const std::string,
               const std::map<const std::string,
                              const Config::Value
                              >
               > Config::DEFAULT_VALUES = {
    {"", {

    }},
    {"display" , {
        {"screen_width", 800},
        {"screen_height", 600},
        {"anti-aliasing", 1},
        {"fullscreen", 0}
    }}
};

static Logger *logger = nullptr;

Config::Config(const fs::path& configFile) : alConfig(nullptr) {
    if(!logger)
        logger = &Logger::getLogger("CONFIG");

    if(configFile != fs::path())
        setConfigFile(configFile);

    if(!checkedDataDir) {
        checkedDataDir = true;
        if(fs::exists(DATA_DIR)) {
            if(fs::is_directory(DATA_DIR))
                return;
            else
                throw std::runtime_error("'" + DATA_DIR.string() + "' is not a directory");
        } else {
            fs::create_directory(DATA_DIR);
        }
    }
}

Config::~Config() {
    cleanup();
}

const fs::path& Config::getConfigFile() const {
    return configFile;
}

void Config::load(const fs::path& newConfig) {
    if(newConfig != fs::path())
        setConfigFile(newConfig);

    logger->info("Loading configuration file: " + configFile.string());

    if(fs::exists(configFile))
        alConfig = al_load_config_file(configFile.c_str());
    else
        alConfig = al_create_config();

    for(auto &it : DEFAULT_VALUES)
        al_add_config_section(alConfig, it.first.c_str());
}

void Config::setConfigFile(const fs::path& newConfig) {
    if(alConfig)
        cleanup();

    configFile = newConfig;
    alConfig = nullptr;
}

void Config::flush() {
    assert(alConfig != nullptr);
    assert(configFile != fs::path());

    al_save_config_file(configFile.c_str(), alConfig);
    logger->info("Saving config file: " + configFile.string());
}

void Config::cleanup() {
    if(!alConfig)
        return;

    flush();

    configFile = fs::path();

    if(al_is_system_installed())
        al_destroy_config(alConfig);
    alConfig = nullptr;
}

void Config::setValue(const Config::Value& newValue, const std::string &key, const std::string &section) {
    csetValue(newValue, key, section);
}

bool Config::isLoaded() const {
    return alConfig != nullptr;
}

void Config::csetValue(const Config::Value& newValue, const std::string& key, const std::string& section) const {
    assert(DEFAULT_VALUES.find(section) != DEFAULT_VALUES.end()); // Check that section valid.
    assert(DEFAULT_VALUES.at(section).find(key) != DEFAULT_VALUES.at(section).end()); // Check that key is valid.
    assert(isLoaded()); // Check that we're actually loaded.

    std::string valueStr;
    switch(newValue.index()) {
        case 0:
            valueStr = std::get<std::string>(newValue);
            break;

        case 1:
            valueStr = std::to_string(std::get<int>(newValue));
            break;

        case 2:
            valueStr = std::to_string(std::get<float>(newValue));
            break;

        default:
            throw std::runtime_error("Default value type unknown: " + std::to_string(newValue.index()));
    }

    al_set_config_value(alConfig, section.c_str(), key.c_str(), valueStr.c_str());
}
