#include "Menu.hpp"

using namespace minicraft;

Menu::Menu(const Colour &colour) :
    defaultColour(colour),
    currentIndex(0) {

}

Menu::~Menu() {
    entries.clear();
}

void Menu::addEntry(MenuEntry &&entry) {
    entries.push_back(std::move(entry));
}

void Menu::setSelectionCallback(const MenuSelectionCallback &callback) {
    selectionCallback = callback;
}

void Menu::render(minicraft::Display &window, const minicraft::Point &) const {
    for(std::size_t i = 0; i < entries.size(); i++) {
        if(i == currentIndex && selectionCallback)
            selectionCallback(entries[i]);
        else
            entries[i].setTextColour(defaultColour);
        entries[i].render(window);
    }
}

void Menu::keyboardCallback(int keyboardCode) {
    switch(keyboardCode) {
        case ALLEGRO_KEY_W:
            if(currentIndex > 0)
                currentIndex--;
            else
                currentIndex = entries.size() - 1;
            break;

        case ALLEGRO_KEY_S:
            if(currentIndex < entries.size() - 1)
                currentIndex++;
            else
                currentIndex = 0;
            break;
    }
}

const MenuEntry &Menu::getSelection() const {
    return entries.at(currentIndex);
}
