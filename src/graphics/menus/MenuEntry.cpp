#include "MenuEntry.hpp"

using namespace minicraft;

MenuEntry::MenuEntry(const Point &p, const std::string &str, const Colour &colour) : pos(p), entryStr(str), textColour(colour) {
}

MenuEntry::MenuEntry(MenuEntry &&mv) noexcept : textColour("#F0F0F0") {
    *this = std::move(mv);
}

MenuEntry &MenuEntry::operator=(MenuEntry &&mv) noexcept {
    std::swap(pos, mv.pos);
    std::swap(entryStr, mv.entryStr);
    std::swap(icon, mv.icon);
    std::swap(textColour, mv.textColour);

    return *this;
}

void MenuEntry::addIcon(BitMap &&icon) {
    this->icon = icon;
}

void MenuEntry::render(Display &window, const Point &) const {
    const Font &font = SpriteSheet::getSpriteFont();
    Point p = pos;
    if(icon.loaded()) {
        icon.render(window, pos);
        p = Point(pos.X() + 1, pos.Y(), pos.getType());
    }
    font.drawText(window, p, entryStr, textColour);
}

const std::string& MenuEntry::getStr() const {
    return entryStr;
}

void MenuEntry::setTextColour(const Colour &textColour) const {
    this->textColour = textColour;
}

const Point &MenuEntry::getPos() const {
    return pos;
}
