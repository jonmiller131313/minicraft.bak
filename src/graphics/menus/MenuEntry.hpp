/**
 * @file MenuEntry.hpp
 * @brief
 */
#ifndef MINICRAFT_MENUENTRY_HPP
#define MINICRAFT_MENUENTRY_HPP

#include <utility>
#include <string>

#include "graphics/BitMap.hpp"
#include "graphics/Colour.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/shapes/Point.hpp"
#include "graphics/SpriteSheet.hpp"

namespace minicraft {

class Menu;
class MenuEntry : public Renderable {
    public:
        MenuEntry(const Point &p, const std::string &str, const Colour &colour = {"#F0F0F0"});

        MenuEntry(const MenuEntry &cpy) = delete;

        MenuEntry(MenuEntry &&mv) noexcept;

        MenuEntry& operator=(const MenuEntry &cpy) = delete;

        MenuEntry& operator=(MenuEntry &&mv) noexcept;

        void addIcon(BitMap &&icon);

        void render(Display &window, const Point &pos = {}) const override;

        const std::string& getStr() const;

        void setTextColour(const Colour &textColour) const;

        const Point& getPos() const;

    private:
        Point pos;
        std::string entryStr;
        BitMap icon;
        mutable Colour textColour;

        friend Menu;
};

}
#endif
