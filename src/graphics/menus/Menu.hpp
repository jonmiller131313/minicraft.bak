/**
 * @file Menu.hpp
 * @brief
 */
#ifndef MINICRAFT_MENU_HPP
#define MINICRAFT_MENU_HPP

#include <functional>
#include <string>
#include <vector>

#include "graphics/Colour.hpp"
#include "graphics/menus/MenuEntry.hpp"
#include "graphics/Renderable.hpp"

namespace minicraft {

using MenuSelectionCallback = std::function<void(MenuEntry &entry)>;

class Menu : public Renderable {
    public:
        Menu(const Colour &colour);

        ~Menu();

        void addEntry(MenuEntry &&entry);

        void setSelectionCallback(const MenuSelectionCallback &callback);

        void render(Display &window, const Point &pos = {}) const override;

        void keyboardCallback(int keyboardCode);

        const MenuEntry& getSelection() const;

    private:
        const Colour defaultColour;

        MenuSelectionCallback selectionCallback;

        mutable std::vector<MenuEntry> entries;

        std::size_t currentIndex;

};

};

#endif
