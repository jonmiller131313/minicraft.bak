/**al_draw_bitmap
 * @file BitMap.hpp
 * @brief
 */
#ifndef MINICRAFT_BITMAP_HPP
#define MINICRAFT_BITMAP_HPP

#include <filesystem>
#include <vector>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>

#include "graphics/Colour.hpp"
#include "graphics/Font.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/shapes/Point.hpp"
#include "Display.hpp"

namespace fs = std::filesystem;

namespace minicraft {

enum RotationType {
    CLOCKWISE,
    COUNTER_CLOCKWISE,
};

enum FlipType {
    HORIZONTAL,
    VERTICAL,
    DIAGONAL
};

class Display;

class BitMap : public Renderable {
    public:
        BitMap();

        BitMap(const BitMap &cpy);

        BitMap(BitMap &&mv) noexcept;

        BitMap &operator=(const BitMap &cpy);

        BitMap &operator=(BitMap &&mv) noexcept;

        bool loadFile(const fs::path &imageFile);

        void createBlank(int w, int h);

        ~BitMap();

        void unload();

        bool loaded() const;

        BitMap createSubBitMap(int x, int y, int w, int h) const;

        operator ALLEGRO_BITMAP *();

        void convertColourToAlpha(const Colour &mask);

        void fill(const Colour &fill);

        void draw(BitMap &from, const Point &pos);

        int width() const;

        int height() const;

        Font createFont(const std::vector<int> &unicodeRanges) const;

        void saveToFile(const fs::path &saveFile) const;

        bool replaceColour(const Colour &before, const Colour &after);

        void rotate(RotationType type);

        void flip(FlipType type);

        void render(Display &window, const Point &pos = {}) const override;

    private:
        ALLEGRO_BITMAP *bitmap;
        std::size_t w;
        std::size_t h;

        int flags;
        float rotationAngle;

        friend Display;

        void updateDimensions();
};

};

#endif
