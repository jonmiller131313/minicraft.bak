/**
 * @file Point.hpp
 * @brief
 */
#ifndef MINICRAFT_POINT_HPP
#define MINICRAFT_POINT_HPP

#include <cstdlib>

namespace minicraft {

class Point {
    public:
        enum Type {
            GRID,
            RAW
        };

        Point();

        Point(std::size_t x, std::size_t y, Type t = GRID);

        Point(const Point &cpy) = default;

        Point &operator=(const Point &cpy) = default;

        friend bool operator==(const Point &lhs, const Point &rhs);

        friend bool operator!=(const Point &lhs, const Point &rhs);

        Point toGrid() const;

        Point toRaw() const;

        std::size_t X() const;

        std::size_t Y() const;

        Type getType() const;

    private:
        std::size_t x;
        std::size_t y;
        Type type;

};

};

#endif
