/**
 * @file Rectangle.hpp
 * @brief
 */
#ifndef MINICRAFT_RECTANGLE_HPP
#define MINICRAFT_RECTANGLE_HPP

#include <map>

#include "graphics/Colour.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/shapes/Point.hpp"
#include "graphics/SpriteSheet.hpp"

#include "Display.hpp"

namespace minicraft {

class Rectangle : public Renderable {
    public:
        static void loadEdges(SpriteSheet &sheet);

        Rectangle(std::size_t gridX, std::size_t gridY, std::size_t w, std::size_t h,
                  const std::map<std::size_t, Colour> &replaceColours = {});

        Rectangle(const Point &ul, const Point &lr, const std::map<std::size_t, Colour> &replaceColours = {});

        void render(Display &window, const Point & = {}) const override;

        const Point upperLeft;
        const Point lowerRight;
        const Point upperRight;
        const Point lowerLeft;

        const std::size_t width;
        const std::size_t height;

    private:
        static BitMap upperLeftBitMap;
        static BitMap upperRightBitMap;
        static BitMap lowerLeftBitMap;
        static BitMap lowerRightBitMap;
        static BitMap topBitMap;
        static BitMap bottomBitMap;
        static BitMap leftBitMap;
        static BitMap rightBitMap;
        static BitMap fillBitMap;

        BitMap bitmap;
};

};

#endif
