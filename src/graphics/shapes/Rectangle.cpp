#include "Rectangle.hpp"

using namespace minicraft;

BitMap Rectangle::upperLeftBitMap;
BitMap Rectangle::upperRightBitMap;
BitMap Rectangle::lowerLeftBitMap;
BitMap Rectangle::lowerRightBitMap;

BitMap Rectangle::topBitMap;
BitMap Rectangle::bottomBitMap;
BitMap Rectangle::leftBitMap;
BitMap Rectangle::rightBitMap;

BitMap Rectangle::fillBitMap;

Rectangle::Rectangle(std::size_t gridX, std::size_t gridY, std::size_t w, std::size_t h,
                     const std::map<std::size_t, Colour> &replaceColours) :
    Rectangle(Point(gridX, gridY), Point(gridX + w, gridY + h), replaceColours) {}

Rectangle::Rectangle(const Point& ul, const Point& lr, const std::map<std::size_t, Colour> &replaceColours) :
    upperLeft(ul.toGrid()),
    lowerRight(lr.toGrid()),
    upperRight(lowerRight.X(), upperLeft.Y()),
    lowerLeft(upperLeft.X(), lowerRight.Y()),
    width(lowerRight.X() - upperLeft.X()),
    height(lowerRight.Y() - upperLeft.Y()) {

    assert(width > 1 && height > 1);

    bitmap.createBlank(width * SpriteSheet::SPRITE_WIDTH, height * SpriteSheet::SPRITE_HEIGHT);
    bitmap.fill(Colour::BLACK);
    bitmap.draw(upperLeftBitMap, {0, 0});
    bitmap.draw(lowerLeftBitMap, {0, height - 1});
    bitmap.draw(upperRightBitMap, {width - 1, 0});
    bitmap.draw(lowerRightBitMap, {width - 1, height - 1});

    for(std::size_t y = 1; y < height - 1; y++) {
        bitmap.draw(leftBitMap, {0, y});
        bitmap.draw(rightBitMap, {width - 1, y});
        for(std::size_t x = 1; x < width - 1; x++) {
            if(y == 1)
                bitmap.draw(topBitMap, {x, 0});
            if(y == height - 2)
                bitmap.draw(bottomBitMap, {x, height - 1});
            bitmap.draw(fillBitMap, {x, y});
        }
    }

    for(const auto &cit : replaceColours) {
        bitmap.replaceColour(SpriteSheet::COLOURS.at(cit.first), cit.second);
    }
}

#include <allegro5/allegro_font.h>
void Rectangle::render(Display& window, const Point&) const {
    al_set_target_backbuffer(window);

    bitmap.render(window, upperLeft);
}

void Rectangle::loadEdges(SpriteSheet& sheet) {
    upperLeftBitMap = sheet.getBitMap(0, 13);

    upperRightBitMap = upperLeftBitMap;
    upperRightBitMap.rotate(CLOCKWISE);

    lowerLeftBitMap = upperLeftBitMap;
    lowerLeftBitMap.flip(VERTICAL);

    lowerRightBitMap = upperRightBitMap;
    lowerRightBitMap.flip(HORIZONTAL);

    topBitMap = sheet.getBitMap(1, 13);

    bottomBitMap = topBitMap;
    bottomBitMap.flip(VERTICAL);

    leftBitMap = topBitMap;
    leftBitMap.rotate(COUNTER_CLOCKWISE);

    rightBitMap = leftBitMap;
    rightBitMap.flip(VERTICAL);

    fillBitMap.createBlank(SpriteSheet::SPRITE_WIDTH, SpriteSheet::SPRITE_HEIGHT);
    fillBitMap.fill(SpriteSheet::COLOURS[1]);

}
