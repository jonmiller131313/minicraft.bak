#include "Point.hpp"

#include "graphics/SpriteSheet.hpp"

using namespace minicraft;

Point::Point() : Point(-1, -1, RAW) {}

Point::Point(std::size_t x, std::size_t y, Point::Type t) : x(x), y(y), type(t) {

}

Point Point::toGrid() const {
    return type == GRID ? *this : Point(x / SpriteSheet::SPRITE_WIDTH, y / SpriteSheet::SPRITE_HEIGHT, GRID);
}

Point Point::toRaw() const {
    return type == RAW ? *this : Point(x * SpriteSheet::SPRITE_WIDTH, y * SpriteSheet::SPRITE_HEIGHT, RAW);
}

size_t Point::X() const {
    return x;
}

size_t Point::Y() const {
    return y;
}

Point::Type Point::getType() const {
    return type;
}

namespace minicraft {

bool operator==(const Point &lhs, const Point &rhs) {
    const Point x = lhs.toRaw();
    const Point y = rhs.toRaw();
    return x.x == y.x && x.y == y.y;
}

bool operator!=(const Point &lhs, const Point &rhs) {
    return !(lhs == rhs);
}

};