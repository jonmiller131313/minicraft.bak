#include "Colour.hpp"

using namespace minicraft;

const Colour Colour::BLACK("#000000");
const Colour Colour::WHITE("#FFFFFF");
const Colour Colour::TRANSPARENT("#00000000");

Colour::Colour(const std::string& hexCode) {
    *this = operator=(hexCode);
}

Colour::Colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    setValues(r, g, b, a);
}

Colour::Colour(const ALLEGRO_COLOR& color) : colour(color) {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
    al_unmap_rgba(color, &r, &g, &b, &a);
    *this = Colour(r, g, b, a);
}

Colour& Colour::operator=(const std::string& newColour) {
    std::string colour(newColour);
    if(newColour.size() != 6 && newColour.size() != 8) {
        if((newColour.size() == 7 || newColour.size() == 9) && newColour[0] == '#')
            colour = newColour.substr(1);
        else
            throw std::runtime_error("Invalid hex code, needs to be of the format \"RRGGBB[AA]\", not:" + newColour);
    }

    std::size_t pos;

    std::string rStr = colour.substr(0, 2);
    uint8_t r = std::stoi(rStr, &pos, 16);
    if(pos != rStr.size())
        throw std::runtime_error("Invalid R hex code: " + rStr);

    std::string gStr = colour.substr(2, 2);
    uint8_t g = std::stoi(gStr, &pos, 16);
    if(pos != gStr.size())
        throw std::runtime_error("Invalid G hex code: " + gStr);

    std::string bStr = colour.substr(4, 2);
    uint8_t b = std::stoi(bStr, &pos, 16);
    if(pos != bStr.size())
        throw std::runtime_error("Invalid G hex code: " + bStr);

    uint8_t a = 0xFF;
    if(colour.size() == 8) {
        std::string aStr = colour.substr(6, 2);
        a = std::stoi(aStr, &pos, 16);
        if(pos != aStr.size())
            throw std::runtime_error("Invalid A hex code: " + aStr);
    }

    hexCode = newColour;
    setValues(r, g, b, a);
    return *this;
}

namespace minicraft {

bool operator==(const Colour &lhs, const Colour &rhs) {
    return lhs.r == rhs.r &&
           lhs.g == rhs.g &&
           lhs.b == rhs.b &&
           lhs.a == rhs.a;
}

};

void Colour::setValues(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
    colour = al_map_rgba(r, g, b, a);
}

Colour::operator const ALLEGRO_COLOR() const {
    return colour;
}


