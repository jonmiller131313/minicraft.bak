#include "SpriteSheet.hpp"

#include "graphics/shapes/Rectangle.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

const Colour SpriteSheet::TRANSPARENT_PINK("#D67FFF");

const std::vector<Colour> SpriteSheet::COLOURS = {
    {"#FFFFFF"}, // White
    {"#ADADAD"}, // Light grey
    {"#515151"}, // Dark grey
    {"#000000"}  // Black
};

Font SpriteSheet::spriteFont;

static Logger *logger = nullptr;

SpriteSheet::SpriteSheet() : width(0), rows(0) {
    if(!logger)
        logger = &Logger::getLogger("SPRITE");
}

SpriteSheet::~SpriteSheet() {
    unload();
}

bool SpriteSheet::loadSheet(const fs::path& spriteFile, std::size_t w, std::size_t r) {
    logger->info("Loading sprite sheet: " + spriteFile.string());

    if(!bitMap.loadFile(spriteFile))
        return false;
    this->spriteFile = spriteFile;
    width = w;
    rows = r;

    for(std::size_t y = 0; y < rows; y++) {
        std::vector<BitMap> row;
        for(std::size_t x = 0; x < width; x++)
            row.push_back(bitMap.createSubBitMap(x * SPRITE_WIDTH, y * SPRITE_HEIGHT,
                                                 SPRITE_WIDTH, SPRITE_HEIGHT));

        spriteGrid.push_back(std::move(row));
    }

    Rectangle::loadEdges(*this);

    loadFont();

    logger->verbose("Sprite sheet loaded: rows=" + std::to_string(rows) + " columns=" + std::to_string(width));

    return true;
}

void SpriteSheet::unload() {
    bitMap.unload();
    spriteFile.clear();
}

void SpriteSheet::loadFont() {
    logger->verbose("Loading font");

    const fs::path FONT_FILE = Config::DATA_DIR/"font.png";
    const std::size_t TOTAL_CHARS =
        26 * 2 + // Alphabet (upper and lower)
        10 +     // Numbers
        19;      // Special chars
    BitMap fontBitMap;
    BitMap tmpBitMap;
    std::vector<int> ranges;
    bool renderFont = true;

    if(fs::exists(FONT_FILE)) {
        logger->verbose("Cache font file exists");
        fontBitMap.loadFile(FONT_FILE);
        renderFont = false;
    }

    if(renderFont) {
        fontBitMap.createBlank(TOTAL_CHARS * (SPRITE_WIDTH + 1) + 1, SPRITE_HEIGHT + 2);
        fontBitMap.fill(TRANSPARENT_PINK);


        for(std::size_t i = 0; i <= 'Z' - 'A' + 1; i++) {
            fontBitMap.draw(spriteGrid[30][i], {i * (SPRITE_WIDTH + 2) - i + 1, 1, Point::RAW});
            if(i != 'Z' - 'A' + 1) // Draw lower case letters
                fontBitMap.draw(spriteGrid[30][i], {i * (SPRITE_WIDTH + 2) - i + 2 +
                                                   (('Z' - 'A') * (SPRITE_WIDTH + 2) - SPRITE_WIDTH),
                                                    1, Point::RAW});
        }
    }
    ranges.push_back('A');
    ranges.push_back('Z');
    ranges.push_back(' ');
    ranges.push_back(' ');
    ranges.push_back('a');
    ranges.push_back('z');

    std::size_t currentWidth;
    if(renderFont) {
        currentWidth = (2 * ('Z' - 'A') + 1) * (SPRITE_WIDTH + 2) - ('Z' - 'A' + SPRITE_WIDTH - 1);

        for(std::size_t i = 0; i <= 9; i++) {
            fontBitMap.draw(spriteGrid[31][i], {currentWidth, 1, Point::RAW});
            currentWidth += SPRITE_WIDTH + 1;
        }
    }
    ranges.push_back('0');
    ranges.push_back('9');

    const std::vector<char> SPECIAL_CHARS = {'.', ',', '!', '?', '\'', '"', '-', '+', '=', '/', '\\', '%', '(', ')', '<', '>', ':', ';'};
    for(std::size_t i = 0; i < SPECIAL_CHARS.size(); i++) {
        if(renderFont) {
            fontBitMap.draw(spriteGrid[31][10 + i], {currentWidth, 1, Point::RAW});
            currentWidth += SPRITE_WIDTH + 1;
        }
        ranges.push_back(SPECIAL_CHARS[i]);
        ranges.push_back(SPECIAL_CHARS[i]);
    }

    if(renderFont) {
        fontBitMap.convertColourToAlpha(Colour::BLACK);
        fontBitMap.saveToFile(FONT_FILE);
    }
    spriteFont = fontBitMap.createFont(ranges);

    logger->verbose(std::to_string(fontBitMap.width() / (SPRITE_WIDTH + 1)) + " characters loaded");
}

BitMap SpriteSheet::getBitMap(const Point &pos,std::size_t w, std::size_t h,  const std::map<std::size_t, Colour> &replaceColours) {
    assert(replaceColours.size() == 0 || replaceColours.rbegin()->first <= 4);

    const Point gridPos = pos.toGrid();
    std::size_t r = gridPos.Y();
    std::size_t c = gridPos.X();
    BitMap tmp;
    tmp.createBlank(SPRITE_WIDTH * w, SPRITE_HEIGHT * h);
    for(std::size_t y = r; y < r + h; y++) {
        for(std::size_t x = c; x < c + w; x++) {
            tmp.draw(spriteGrid[y][x], {x - c, y - r});
        }
    }
    for(const auto &it : replaceColours) {
        tmp.replaceColour(COLOURS[it.first], it.second);
    }
    return tmp;
}

BitMap SpriteSheet::getBitMap(std::size_t x, std::size_t y) {
    assert(x < width && y < rows);
    BitMap tmp(spriteGrid[y][x]);
    return tmp;
}

const Font &SpriteSheet::getSpriteFont() {
    return spriteFont;
}
