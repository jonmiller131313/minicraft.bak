/**
 * @file Colour.hpp
 * @brief
 */
#ifndef MINICRAFT_COLOUR_HPP
#define MINICRAFT_COLOUR_HPP

#include <stdexcept>
#include <string>

#include <cstdint>

#include <allegro5/allegro5.h>

namespace minicraft {

class Colour {
    public:
        const static Colour BLACK;
        const static Colour WHITE;
        const static Colour TRANSPARENT;


        Colour(const std::string &hexCode);

        Colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF);

        Colour(const ALLEGRO_COLOR &color);

        Colour& operator=(const std::string &newColour);

        friend bool operator==(const Colour &lhs, const Colour &rhs);

        void setValues(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF);

        operator const ALLEGRO_COLOR() const;

    private:
        std::string hexCode;

        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;

        ALLEGRO_COLOR colour;
};

};

#endif
