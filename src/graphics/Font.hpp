/**
 * @file Font.hpp
 * @brief
 */
#ifndef MINICRAFT_FONT_HPP
#define MINICRAFT_FONT_HPP

#include <functional>
#include <memory>
#include <stdexcept>
#include <string>

#include <allegro5/allegro_font.h>

#include "graphics/shapes/Point.hpp"
#include "Display.hpp"

namespace minicraft {

class Font {
    public:
        Font(ALLEGRO_FONT *font = nullptr);

        Font(const Font &cpy) = delete;

        ~Font();

        Font &operator=(const Font &cpy) = delete;

        Font &operator=(Font &&mv) noexcept;


        static const ALLEGRO_FONT *getBuiltinFont();

        operator ALLEGRO_FONT *() const;

        void drawText(Display &window,
                      const Point &pos,
                      const std::string &message,
                      const Colour &colour = Colour::WHITE) const;

    private:
        ALLEGRO_FONT *font;
};

};

#endif
