/**
 * @file SpriteSheet.hpp
 * @brief
 */
#ifndef MINICRAFT_SPRITESHEET_HPP
#define MINICRAFT_SPRITESHEET_HPP

#include <filesystem>
#include <map>
#include <vector>

#include <jonmiller131313/logging.hpp>

#include "Config.hpp"
#include "graphics/BitMap.hpp"
#include "graphics/Font.hpp"

namespace fs = std::filesystem;

namespace minicraft {

class HelloWorldState;

class SpriteSheet {
    public:
        static const std::size_t SPRITE_WIDTH = 8;
        static const std::size_t SPRITE_HEIGHT = 8;
        static const Colour TRANSPARENT_PINK;
        static const std::vector<Colour> COLOURS;

        SpriteSheet();

        ~SpriteSheet();

        bool loadSheet(const fs::path &spriteFile, std::size_t w, std::size_t r);

        void unload();
        BitMap getBitMap(std::size_t x, std::size_t y);

        BitMap getBitMap(const Point &pos, std::size_t w, std::size_t h,
                         const std::map<std::size_t, Colour> &replaceColours = {});

        static const Font& getSpriteFont();

    private:
        static Font spriteFont;

        fs::path spriteFile;
        BitMap bitMap;
        size_t width;
        size_t rows;

        std::vector<std::vector<BitMap>> spriteGrid;


        void loadFont();

        friend HelloWorldState;
};

};

#endif
