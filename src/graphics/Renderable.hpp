/**
 * @file Renderable.hpp
 * @brief
 */
#ifndef MINICRAFT_RENDERABLE_HPP
#define MINICRAFT_RENDERABLE_HPP

#include "graphics/shapes/Point.hpp"
#include "Display.hpp"

namespace minicraft {

class Renderable {
    public:
        virtual ~Renderable() = default;

        virtual void render(Display &window, const Point &pos = {}) const = 0;
};

};

#endif
