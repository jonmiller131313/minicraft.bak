#include "BitMap.hpp"

#include "graphics/SpriteSheet.hpp"

using namespace minicraft;

static const float PI = 3.1415927;
static const float RAD_CLOCKWISE = PI / 2;
static const float RAD_COUNTER_CLOCKWISE = - PI / 2;

BitMap::BitMap() : bitmap(nullptr), w(0), h(0), flags(0), rotationAngle(0) {

}

BitMap::BitMap(const BitMap& cpy) : bitmap(nullptr), w(0), h(0), flags(0), rotationAngle(0) {
    *this = cpy;
}

BitMap::BitMap(BitMap&& mv) noexcept : bitmap(nullptr), w(0), h(0), flags(0), rotationAngle(0) {
    *this = std::move(mv);
}

BitMap& BitMap::operator=(const BitMap& cpy) {
    if(this == &cpy)
        return *this;

    unload();

    BitMap tmp(cpy.createSubBitMap(0, 0, cpy.w, cpy.h));
    *this = std::move(tmp);
    return *this;
}

BitMap& BitMap::operator=(BitMap&& mv) noexcept {
    std::swap(bitmap, mv.bitmap);
    std::swap(w, mv.w);
    std::swap(h, mv.h);
    std::swap(flags, mv.flags);
    std::swap(rotationAngle, mv.rotationAngle);
    return *this;
}

bool BitMap::loadFile(const fs::path &imageFile) {
    if(!fs::exists(imageFile) || !fs::is_regular_file(imageFile))
        throw std::runtime_error("'" + imageFile.string() + "' is not a loadable image");

    bitmap = al_load_bitmap(imageFile.c_str());
    updateDimensions();
    return bitmap != nullptr;
}

BitMap::~BitMap() {
    unload();
}

void BitMap::unload() {
    if(bitmap) {
        if(al_is_system_installed())
            al_destroy_bitmap(bitmap);
        bitmap = nullptr;
        w = 0;
        h = 0;
        flags = 0;
        rotationAngle = 0;
    }
}

bool BitMap::loaded() const {
    return bitmap != nullptr;
}

BitMap BitMap::createSubBitMap(int x, int y, int w, int h) const {
    BitMap subMap;
    if(!loaded())
        return subMap;

    subMap.bitmap = al_create_sub_bitmap(bitmap, x, y, w, h);
    if(!subMap.bitmap)
        throw std::runtime_error("Unable to create sub-bitmap");

    subMap.w = w;
    subMap.h = h;
    subMap.flags = flags;
    subMap.rotationAngle = rotationAngle;

    return subMap;
}

BitMap::operator ALLEGRO_BITMAP*() {
    return bitmap;
}

void BitMap::convertColourToAlpha(const Colour& mask) {
    if(!loaded())
        return;

    al_convert_mask_to_alpha(bitmap, mask);
}

Font BitMap::createFont(const std::vector<int> &unicodeRanges) const {
    assert(loaded());
    assert(!unicodeRanges.empty());
    assert(unicodeRanges.size() % 2 == 0);
    return Font(al_grab_font_from_bitmap(bitmap, static_cast<int>(unicodeRanges.size() / 2), unicodeRanges.data()));
}

void BitMap::createBlank(int w, int h) {
    bitmap = al_create_bitmap(w,  h);
    updateDimensions();
}

void BitMap::draw(BitMap& from, const Point &pos) {
    assert(loaded());
    assert(from.loaded());
    Point rawPos = pos.toRaw();
    assert(rawPos.X() < w && rawPos.Y() < h);

    int x = rawPos.X();
    int y = rawPos.Y();

    if(from.rotationAngle == RAD_CLOCKWISE)
        x += from.w;
    else if(from.rotationAngle == RAD_COUNTER_CLOCKWISE)
        y += from.h;

    al_set_target_bitmap(bitmap);
    al_draw_rotated_bitmap(from, 0, 0, x, y, from.rotationAngle, from.flags);
}

int BitMap::width() const {
    assert(loaded());
    return w;
}

int BitMap::height() const {
    assert(loaded());
    return h;
}

void BitMap::fill(const Colour& fill) {
    assert(loaded());

    al_set_target_bitmap(bitmap);
    al_clear_to_color(fill);
}

void BitMap::saveToFile(const fs::path& saveFile) const {
    assert(loaded());

    al_save_bitmap(saveFile.c_str(), bitmap);
}

bool BitMap::replaceColour(const Colour& before, const Colour& after) {
    assert(loaded());
    ALLEGRO_LOCKED_REGION *lockedRegion = al_lock_bitmap(bitmap, al_get_bitmap_format(bitmap), ALLEGRO_LOCK_READWRITE);
    if(!lockedRegion)
        return false;

    al_set_target_bitmap(bitmap);

    bool found = false;
    for(std::size_t x = 0; x < w; x++) {
        for(std::size_t y = 0; y < h; y++) {
            Colour pixelColour = al_get_pixel(bitmap, x, y);
            if(pixelColour == before) {
                al_put_pixel(x, y, after);
                found = true;
            }
        }
    }
    al_unlock_bitmap(bitmap);

    return found;
}

void BitMap::updateDimensions() {
    assert(loaded());
    w = al_get_bitmap_width(bitmap);
    h = al_get_bitmap_height(bitmap);
}

void BitMap::rotate(RotationType type) {

    rotationAngle = type == CLOCKWISE ? RAD_CLOCKWISE : RAD_COUNTER_CLOCKWISE;
}

void BitMap::flip(FlipType type) {
    switch(type) {
        case HORIZONTAL:
            flags ^= ALLEGRO_FLIP_HORIZONTAL;
            break;

        case VERTICAL:
            flags ^= ALLEGRO_FLIP_VERTICAL;
            break;

        case DIAGONAL:
            flags ^= ALLEGRO_FLIP_HORIZONTAL | ALLEGRO_FLIP_VERTICAL;
            break;
    }
}

void BitMap::render(Display& window, const Point &pos) const {
    assert(loaded());
    al_set_target_backbuffer(window);
    Point rawPos = pos.toRaw();
    float x = rawPos.X();
    float y = rawPos.Y();
    if(rotationAngle == RAD_CLOCKWISE)
        x++;
    else if(rotationAngle == RAD_COUNTER_CLOCKWISE)
        y++;
    al_draw_rotated_bitmap(bitmap, 0, 0, x, y, rotationAngle, flags);
}
