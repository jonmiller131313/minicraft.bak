#include "Font.hpp"

using namespace minicraft;

class BuiltinFont {
    public:
        BuiltinFont() : font(al_create_builtin_font()) {
            if(!font)
                throw std::runtime_error("Unable to load builtin Allegro font");
        }

        ~BuiltinFont() {
            if(font && al_is_system_installed())
                al_destroy_font(font);
        }

        operator const ALLEGRO_FONT*() const {
            return font;
        }

    private:
        ALLEGRO_FONT *font;
};

Font::Font(ALLEGRO_FONT *font) : font(font) {

}

Font::~Font() {
    if(font && al_is_system_installed())
        al_destroy_font(font);
}

Font& Font::operator=(Font&& mv) noexcept {
    std::swap(font, mv.font);
    return *this;
}

const ALLEGRO_FONT *Font::getBuiltinFont() {
    static BuiltinFont builtinFont;
    return builtinFont;
}

Font::operator ALLEGRO_FONT*() const {
    return font;
}

void Font::drawText(Display& window, const Point& pos, const std::string& message, const Colour& colour) const {
    al_set_target_backbuffer(window);
    const Point &rawPos = pos.toRaw();
    al_draw_text(font, colour, rawPos.X(), rawPos.Y(), 0, message.c_str());
}
