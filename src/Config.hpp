/**
 * @file Config.hpp
 * @brief Managing game configuration.
 */
#ifndef MINICRAFT_CONFIG_HPP
#define MINICRAFT_CONFIG_HPP

#include <algorithm>
#include <filesystem>
#include <map>
#include <string>
#include <variant>
#include <vector>

#include <allegro5/allegro.h>

#include <jonmiller131313/logging.hpp>
#include <jonmiller131313/xdg_dirs.hpp>

namespace fs = std::filesystem;

namespace minicraft {

class Config {
    public:
        using Value = std::variant<std::string, int, float>;

        static const fs::path DATA_DIR;

        Config(const fs::path &configFile = fs::path());

        ~Config();

        void load(const fs::path &newConfig = fs::path());

        void setConfigFile(const fs::path &newConfig);

        const fs::path &getConfigFile() const;

        bool isLoaded() const;

        void flush();

        void cleanup();

        template <typename T>
        T getValue(const std::string &key, const std::string &section = "") const {
            assert(DEFAULT_VALUES.find(section) != DEFAULT_VALUES.end()); // Check that section valid.
            assert(
                DEFAULT_VALUES.at(section).find(key) != DEFAULT_VALUES.at(section).end()); // Check that key is valid.
            assert(isLoaded()); // Check that we're actually loaded.

            Value value = DEFAULT_VALUES.at(section).at(key);
            const char *v = al_get_config_value(alConfig, section.c_str(), key.c_str());
            if(!v) {
                csetValue(value, key, section);
                return std::get<T>(value);
            }

            std::string vStr(v);

            size_t p;
            int i = std::stoi(vStr, &p);
            if(p == vStr.size()) {
                value = i;
                return std::get<T>(value);
            }

            float f = std::stof(vStr, &p);
            if(p == vStr.size()) {
                value = f;
                return std::get<T>(value);
            }

            value = vStr;
            return std::get<int>(value);
        }

        void setValue(const Value &newValue, const std::string &key, const std::string &section = "");

    private:
        static const std::map<
            const std::string, const std::map<
                const std::string, const Value
            >
        > DEFAULT_VALUES;

        fs::path configFile;

        ALLEGRO_CONFIG *alConfig;

        void csetValue(const Value &newValue, const std::string &key, const std::string &section = "") const;

};

};

#endif
