#include "GameState.hpp"

#include <thread>

#include <X11/Xlib.h>

#include <allegro5/allegro_x.h>

#include "graphics/shapes/Rectangle.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

Logger *GameState::logger = nullptr;

GameState::GameState(Game& game) :
        redraw(true),
        done(false),
        game(game),
        mainWindow(game.mainWindow),
        spriteSheet(game.spriteSheet),
        spriteFont(SpriteSheet::getSpriteFont()),
        nextState(game.nextState) {
    if(!logger)
        logger = &Logger::getLogger("STATE");

    eventQueue.createQueue();

    renderTimer.createTimer(ALLEGRO_BPS_TO_SECS(30));
}

GameState::~GameState() {
    clearKeyboardCallbacks();
}

bool GameState::isDone() const {
    return done;
}

void GameState::init_state() {
    eventQueue.registerEvent(&mainWindow);
    eventQueue.registerEvent(&renderTimer);

    eventQueue.registerCallback(ALLEGRO_EVENT_TIMER, [this] (ALLEGRO_EVENT *) {
        static Rectangle rect(8, 11, 17, 3, {
            {0, {"#C3C3DB"}},
            {1, {"#161689"}},
            {2, {"#0B0B23"}},
            {3, Colour::TRANSPARENT},
        });
        static ::Display *disp = XOpenDisplay(nullptr);
        ::Window focus;
        int revert;
        bool toggle = true;
        unsigned int count = 0;

        this->redraw = true;

        XGetInputFocus(disp, &focus, &revert);

        for(XID id = al_get_x_window_id(this->mainWindow);
            focus != id;
            XGetInputFocus(disp, &focus, &revert)) {

            rect.render(this->mainWindow);
            spriteFont.drawText(this->mainWindow, {9, 12}, "Click to focus!",
                                { toggle ? "#F0F0F0" : "#949494"});
            if(count++ % 3 == 0)
                toggle = !toggle;

            this->mainWindow.render();

            std::this_thread::sleep_for(static_cast<std::chrono::milliseconds>(110));
        }
    });

    renderTimer.start();
}
