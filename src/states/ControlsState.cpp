#include "ControlsState.hpp"

#include "states/MainMenuState.hpp"

using namespace minicraft;

ControlsState::ControlsState(Game &game) :
    GameState(game) {

}

ControlsState::~ControlsState() {

}

void ControlsState::init_state() {
    GameState::init_state();

    registerKeyboardCallback([this] (int keycode) {
        if(keycode == ALLEGRO_KEY_SPACE ||
           keycode == ALLEGRO_KEY_ENTER ||
           keycode == ALLEGRO_KEY_ESCAPE) {
            this->done = true;
            nextState = std::make_unique<MainMenuState>(game);
        }
    });

    logger->info("Control info state initialized");
}

void ControlsState::main_loop() {
    static const Colour TITLE_COLOUR("#F0F0F0");
    static const Colour CONTROLS_COLOUR("#949494");
    static const std::string TITLE = "Controls";
    static const std::vector<std::string> LINES = {
        "Move:",
        "     Up - W",
        "   Down - S",
        "   Left - A",
        "  Right - D",
        "",
        "Attack/Select - Space/Enter",
        "",
        "    Inventory - E",
        "",
        "   Pause/Back - Escape"
    };

    eventQueue.waitAndHandleEvent();

    if(done)
        return;

    if(redraw && eventQueue.isEmpty()) {
        mainWindow.clearScreen({"#0A0A0A"});

        spriteFont.drawText(mainWindow, {3, 1}, TITLE, TITLE_COLOUR);
        for(std::size_t i = 0; i < LINES.size(); i++)
            spriteFont.drawText(mainWindow, {1, 3 + i}, LINES[i], CONTROLS_COLOUR);

        redraw = false;
        mainWindow.render();
    }
}
