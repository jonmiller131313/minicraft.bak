/**
 * @file ControlsState.hpp
 * @brief
 */
#ifndef MINICRAFT_CONTROLSSTATE_HPP
#define MINICRAFT_CONTROLSSTATE_HPP

#include "Game.hpp"
#include "states/GameState.hpp"

namespace minicraft {

class ControlsState : public GameState {
    public:
        ControlsState(Game &game);

        ~ControlsState();

        void init_state() override;

        void main_loop() override;

    private:
};

};

#endif
