#include "HelloWorldState.hpp"

#include <allegro5/allegro_primitives.h>

using namespace minicraft;

static float x = 100;
static float y = 100;
static char buffer[100];

HelloWorldState::HelloWorldState(Game& game) : GameState(game) {

}

HelloWorldState::~HelloWorldState() {

}

void HelloWorldState::init_state() {
    GameState::init_state();

    eventQueue.registerCallback(ALLEGRO_EVENT_TIMER, [this] (ALLEGRO_EVENT *) {
        this->redraw = true;
    });

//    registerKeyboardCallback(ALLEGRO_KEY_UP, [] (uint8_t) {
//        y--;
//    });
//    registerKeyboardCallback(ALLEGRO_KEY_DOWN, [] (uint8_t) {
//        y++;
//    });
//    registerKeyboardCallback(ALLEGRO_KEY_LEFT, [] (uint8_t) {
//        x--;
//    });
//    registerKeyboardCallback(ALLEGRO_KEY_RIGHT, [] (uint8_t) {
//        x++;
//    });
//
//    registerKeyboardCallback(ALLEGRO_KEY_ESCAPE, [this] (uint8_t) {
//        this->done = true;
//    }, ONESHOT);

    eventQueue.registerCallback(ALLEGRO_EVENT_DISPLAY_CLOSE, [this] (ALLEGRO_EVENT*) {
        this->done = true;
    });

    logger->info("Hello world state initialized");
}

void HelloWorldState::main_loop() {
    eventQueue.waitAndHandleEvent();

    if(done)
        return;

    if(redraw && eventQueue.isEmpty()) {
        mainWindow.clearScreen();

        sprintf(buffer, "X: %.1f Y: %.1f", x, y);

        spriteFont.drawText(mainWindow, {0, 0}, buffer);

        al_draw_filled_rectangle(x, y, x + SpriteSheet::SPRITE_WIDTH, y + SpriteSheet::SPRITE_HEIGHT, Colour("#FF0000"));


        redraw = false;
        mainWindow.render();
    }
}

