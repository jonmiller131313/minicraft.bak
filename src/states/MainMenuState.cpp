#include "MainMenuState.hpp"

#include "states/AboutState.hpp"
#include "states/ControlsState.hpp"

using namespace minicraft;

MainMenuState::MainMenuState(Game& game) :
    GameState(game),
    titleBitmap(spriteSheet.getBitMap({0, 6}, 13, 2, {
        {0, {"#E5E589"}},
        {1, {"#538153"}},
        {2, {"#1C334A"}},
        {3, Colour::TRANSPARENT}
    })),
    mainMenu({"#666666"}) {
}

MainMenuState::~MainMenuState() {
    callbacks.clear();
}

void MainMenuState::init_state() {
    GameState::init_state();

    eventQueue.registerCallback(ALLEGRO_EVENT_DISPLAY_CLOSE, [this] (ALLEGRO_EVENT *) {
        this->done = true;
    });

    callbacks.emplace_back(
        "Start Game",
        [this] () {
            this->selectedStartGame();

        }
    );
    callbacks.emplace_back(
        "Controls",
        [this] () {
            this->selectedControls();
        }
    );
    callbacks.emplace_back(
        "About",
        [this] () {
            this->selectedAbout();
        }
    );
    callbacks.emplace_back(
        "Quit",
        [this] () {
            this->done = true;
        }
    );

    std::size_t y = 8;
    for(const auto &pair : callbacks) {
        mainMenu.addEntry({{getHalfStart(pair.first.size() * SpriteSheet::SPRITE_WIDTH),
                                      y++ * SpriteSheet::SPRITE_HEIGHT,
                                      Point::RAW},
                                  pair.first});
    }

    mainMenu.setSelectionCallback([this] (MenuEntry &entry) {
        const Colour selectionColour("#F0F0F0");
        Point lPos = entry.getPos().toRaw();
        lPos = Point(lPos.X() - (2 * SpriteSheet::SPRITE_WIDTH), lPos.Y(), Point::RAW);
        Point rPos = Point(lPos.X() + (3 + entry.getStr().size()) * SpriteSheet::SPRITE_WIDTH, lPos.Y(), Point::RAW);
        this->spriteFont.drawText(this->mainWindow, lPos, ">", selectionColour);
        this->spriteFont.drawText(this->mainWindow, rPos, "<", selectionColour);
        entry.setTextColour(selectionColour);
    });

    registerKeyboardCallback([this] (int keycode) {
        this->mainMenu.keyboardCallback(keycode);
        if(keycode == ALLEGRO_KEY_SPACE || keycode == ALLEGRO_KEY_ENTER) {
            const MenuEntry &entry = this->mainMenu.getSelection();
            auto it = std::find_if(callbacks.cbegin(), callbacks.cend(), [&entry] (const auto &pair) {
                return pair.first == entry.getStr();
            });
            it->second();
        }
    }, ONESHOT);

    logger->info("Main menu state initialized");
}

void MainMenuState::main_loop() {
    static const Point titlePos(getHalfStart(titleBitmap.width()), 3 * SpriteSheet::SPRITE_HEIGHT, Point::RAW);
    static const Point versionPos(mainWindow.width() / SpriteSheet::SPRITE_WIDTH - Game::VERSION_STR.size(),
                                  mainWindow.height() / SpriteSheet::SPRITE_HEIGHT - 1);


    eventQueue.waitAndHandleEvent();

    if(done)
        return;

    if(redraw && eventQueue.isEmpty()) {
        mainWindow.clearScreen({"#0A0A0A"});

        titleBitmap.render(mainWindow, titlePos);

        mainMenu.render(mainWindow);

        spriteFont.drawText(mainWindow, versionPos, Game::VERSION_STR, {"#383838"});

        redraw = false;
        mainWindow.render();
    }
}

std::size_t MainMenuState::getHalfStart(std::size_t w) {
    return (mainWindow.width() - w) / 2;
}

void MainMenuState::selectedStartGame() {

}

void MainMenuState::selectedControls() {
    done = true;
    nextState = std::make_unique<ControlsState>(game);
}

void MainMenuState::selectedAbout() {
    done = true;
    nextState = std::make_unique<AboutState>(game);
}

