/**
 * @file AboutState.hpp
 * @brief
 */
#ifndef MINICRAFT_ABOUTSTATE_HPP
#define MINICRAFT_ABOUTSTATE_HPP

#include "Game.hpp"
#include "states/GameState.hpp"

namespace minicraft {

class AboutState : public GameState {
    public:
        AboutState(Game &game);

        ~AboutState();

        void init_state() override;

        void main_loop() override;

    private:
};

};

#endif
