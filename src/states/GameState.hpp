/**
 * @file GameState.hpp
 * @brief
 */
#ifndef MINICRAFT_GAMESTATE_HPP
#define MINICRAFT_GAMESTATE_HPP

#include <jonmiller131313/logging.hpp>

#include "events/keyboard_handling.hpp"

#include "Game.hpp"

namespace minicraft {

class GameState {
    public:
        GameState(Game &game);

        virtual ~GameState();

        bool isDone() const;

        virtual void init_state();

        virtual void main_loop() = 0;

    protected:
        static jonmiller131313::logging::Logger *logger;

        bool redraw;
        bool done;
        EventQueue eventQueue;
        Timer renderTimer;

        Game &game;
        Display &mainWindow;
        SpriteSheet &spriteSheet;
        const Font &spriteFont;
        std::unique_ptr<GameState> &nextState;
};

};

#endif
