/**
 * @file MainMenuState.hpp
 * @brief
 */
#ifndef MINICRAFT_HELLOWORLDSTATE_HPP
#define MINICRAFT_HELLOWORLDSTATE_HPP

#include "events/keyboard_handling.hpp"

#include "graphics/shapes/Rectangle.hpp"

#include "states/GameState.hpp"
#include "states/MainMenuState.hpp"

namespace minicraft {

class HelloWorldState : public GameState {
    public:
        HelloWorldState(Game &game);

        ~HelloWorldState();

        void init_state() override;

        void main_loop() override;

    private:
};

};

#endif
