/**
 * @file MainMenuState.hpp
 * @brief
 */
#ifndef MINICRAFT_MAINMENUSTATE_HPP
#define MINICRAFT_MAINMENUSTATE_HPP

#include <functional>
#include <string>
#include <utility>
#include <vector>

#include "events/keyboard_handling.hpp"

#include "graphics/BitMap.hpp"
#include "graphics/menus/Menu.hpp"
#include "graphics/menus/MenuEntry.hpp"

#include "states/GameState.hpp"
#include "states/MainMenuState.hpp"

namespace minicraft {

class MainMenuState : public GameState {
    public:
        MainMenuState(Game &game);

        ~MainMenuState();

        void init_state() override;

        void main_loop() override;

    private:
        const BitMap titleBitmap;

        std::vector<std::pair<std::string, std::function<void()>>> callbacks;

        Menu mainMenu;

        std::size_t getHalfStart(std::size_t w);

        void selectedStartGame();

        void selectedControls();

        void selectedAbout();
};

};

#endif
