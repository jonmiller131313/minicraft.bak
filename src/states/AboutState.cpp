#include "AboutState.hpp"

#include "states/MainMenuState.hpp"

using namespace minicraft;

AboutState::AboutState(Game &game) :
    GameState(game) {

}

AboutState::~AboutState() {

}

void AboutState::init_state() {
    GameState::init_state();

    registerKeyboardCallback([this] (int keycode) {
        if(keycode == ALLEGRO_KEY_SPACE ||
           keycode == ALLEGRO_KEY_ENTER ||
           keycode == ALLEGRO_KEY_ESCAPE) {
            this->done = true;
            nextState = std::make_unique<MainMenuState>(game);
        }
    });

    logger->info("About state initialized");
}

void AboutState::main_loop() {
    static const Colour TITLE_COLOUR("#F0F0F0");
    static const Colour  TEXT_COLOUR("#949494");
    static const Colour  NOTE_COLOUR("#383838");
    static const std::string TITLE = "About Minicraft";
    static const std::vector<std::string> LINES = {
        "Minicraft was made",
        "by Markus Persson",
        "for the 22'nd ludum",
        "dare competition in",
        "December 2011.",
        "",
        "It is dedicated to",
        "my father. <3"
    };
    static const std::vector<std::string> NOTES = {
        "Slightly modified and improved",
        "by Jonathan Miller"
    };

    eventQueue.waitAndHandleEvent();

    if(done)
        return;

    if(redraw && eventQueue.isEmpty()) {
        mainWindow.clearScreen({"#0A0A0A"});

        spriteFont.drawText(mainWindow, {3, 1}, TITLE, TITLE_COLOUR);
        for(std::size_t i = 0; i < LINES.size(); i++)
            spriteFont.drawText(mainWindow, {1, 3 + i}, LINES[i], TEXT_COLOUR);
        for(std::size_t i = 0; i < NOTES.size(); i++)
            spriteFont.drawText(mainWindow, {1, mainWindow.height() / SpriteSheet::SPRITE_HEIGHT - 1 - NOTES.size() + i}, NOTES[i], NOTE_COLOUR);

        redraw = false;
        mainWindow.render();
    }
}
