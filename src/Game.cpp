#include "Game.hpp"

#include "states/HelloWorldState.hpp"
#include "states/MainMenuState.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

const std::string Game::VERSION_STR = "v0.0.0";

static Logger *logger = nullptr;

Game::Game() {
    if(!logger)
        logger = &Logger::getLogger("GAME");

    readConfig();

    mainWindow.createDisplay(config);

    spriteSheet.loadSheet(Config::DATA_DIR / "sprites.png", 32, 32);
}

Game::~Game() {
}

void Game::readConfig() {
    fs::path configDir(xdg_dirs_config_home());
    configDir /= "Minicraft";
    logger->verbose("Config directory: " + configDir.string());

    if(!fs::exists(configDir)) {
        logger->verbose("Configuration directory does not exist, creating");
        fs::create_directory(configDir);
    } else if(!fs::is_directory(configDir)) {
        logger->error("Configuration directory '" + configDir.string() + "' is not a directory");
        throw std::runtime_error("'" + configDir.string() + "' is not a directory.");
    }

    config.load(configDir / "config.ini");
}

void Game::run() {
    logger->info("Game started");

    currentState = std::make_unique<MainMenuState>(*this);
    currentState->init_state();

    while(true) {
        currentState->main_loop();

        if(currentState->isDone()) {
            currentState.swap(nextState);
            nextState.reset();
            if(!currentState)
                break;
            else
                logger->debug("Switching state");

            currentState->init_state();
        }
    }
    logger->info("Game quitting");
}
