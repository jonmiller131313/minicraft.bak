/**
 * @file EventQueue.hpp
 * @brief
 */
#ifndef MINICRAFT_EVENTQUEUE_HPP
#define MINICRAFT_EVENTQUEUE_HPP

#include <functional>
#include <map>
#include <vector>

#include <allegro5/allegro5.h>

#include "events/EventGenerator.hpp"

namespace minicraft {

using EventCallback = std::function<void(ALLEGRO_EVENT *)>;

class EventQueue {
    public:
        EventQueue();

        ~EventQueue();

        bool createQueue(bool shouldRegisterKeyboard = true);

        void registerEvent(const EventGenerator *generator);

        bool isEmpty() const;

        void registerCallback(ALLEGRO_EVENT_TYPE type, const EventCallback &callback);

        void clearCallbacks(ALLEGRO_EVENT_TYPE type);

        void waitAndHandleEvent() const;

        void clear(bool reinit = true);

    private:
        std::map<ALLEGRO_EVENT_TYPE, std::vector<EventCallback>> callbacks;

        ALLEGRO_EVENT_QUEUE *queue;

        friend void registerKeyboardEvents(EventQueue &);
};

};

#endif
