/**
 * @file Game.hpp
 * @brief The main game object
 */
#ifndef MINICRAFT_GAME_HPP
#define MINICRAFT_GAME_HPP

#include <filesystem>
#include <memory>
#include <string>
#include <vector>

#include <jonmiller131313/logging.hpp>
#include <jonmiller131313/xdg_dirs.hpp>

#include "Config.hpp"
#include "EventQueue.hpp"
#include "events/Timer.hpp"
#include "graphics/Font.hpp"
#include "graphics/SpriteSheet.hpp"
#include "Display.hpp"

namespace minicraft {

class GameState;

class Game {
    public:
        const static std::string VERSION_STR;

        Game();

        ~Game();

        void run();

    private:
        Config config;

        Display mainWindow;
        SpriteSheet spriteSheet;

        std::unique_ptr<GameState> currentState;
        std::unique_ptr<GameState> nextState;

        void readConfig();

        friend GameState;
};

};

#endif
