#include "EventQueue.hpp"

#include "events/keyboard_handling.hpp"

using namespace minicraft;

EventQueue::EventQueue() : queue(nullptr) {

}

EventQueue::~EventQueue() {
    clear(false);
}

bool EventQueue::createQueue(bool shouldRegisterKeyboard) {
    if(queue)
        return true;

    queue = al_create_event_queue();
    if(!queue) {
        return false;
    } else if(shouldRegisterKeyboard) {
        registerKeyboardEvents(*this);
    }

    return true;
}

void EventQueue::registerEvent(const EventGenerator *generator) {
    if(!queue)
        createQueue();

    assert(generator != nullptr);
    al_register_event_source(queue, generator->getEventSource());
}

bool EventQueue::isEmpty() const {
    assert(queue != nullptr);
    return al_is_event_queue_empty(queue);
}

void EventQueue::registerCallback(ALLEGRO_EVENT_TYPE type, const EventCallback& callback) {
    if(!queue)
        createQueue();
    callbacks[type].push_back(callback);
}

void EventQueue::waitAndHandleEvent() const {
    assert(queue != nullptr);

    ALLEGRO_EVENT event;
    al_wait_for_event(queue, &event);

    auto search = callbacks.find(event.type);
    if(search != callbacks.end())
        for(const EventCallback &callback : search->second)
            callback(&event);
}

void EventQueue::clear(bool reinit) {
    if(queue && al_is_system_installed()) {
        al_destroy_event_queue(queue);
    }
    if(reinit)
        queue = al_create_event_queue();
    else
        queue = nullptr;
}

void EventQueue::clearCallbacks(ALLEGRO_EVENT_TYPE type) {
    auto search = callbacks.find(type);
    if(search != callbacks.end())
        search->second.clear();
}
