#include <cstdio>
#include <cstdlib>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>

#include <jonmiller131313/logging.hpp>

#include "Game.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

bool initializeAllegro() {
    Logger &logger = Logger::getLogger("ALLEGRO");

    logger.debug("Initializing Allegro library");
    if(!al_init()) {
        logger.fatal("Unable to initialize allegro");
        return false;
    } else if(!al_install_keyboard()) {
        logger.fatal("Unable to initialize allegro keyboard drivers");
        return false;
    } else if(!al_init_font_addon()) {
        logger.fatal("Unable to initialize allegro font addon");
        return false;
    } else if(!al_init_image_addon()) {
        logger.fatal("Unable to initialize allegro image addon");
    }
    logger.verbose("Allegro library initialized successfully");
    return true;
}

int main() {
    Logger::globalConfig("[%10N:%7T] [%D] [%20F:%4l] ", "%F %T", DEBUG);

    if(!initializeAllegro())
        return EXIT_FAILURE;

    Game game;

    game.run();

    return EXIT_SUCCESS;
}
