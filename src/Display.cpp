#include "Display.hpp"

#include "graphics/BitMap.hpp"

using namespace jonmiller131313::logging;

using namespace minicraft;

static Logger *logger = nullptr;

Display::Display() : disp(nullptr), w(0), h(0) {
    if(!logger)
        logger = &Logger::getLogger("DISPLAY");
}

Display::~Display() {
    if(disp && al_is_system_installed())
        al_destroy_display(disp);
}

bool Display::createDisplay(const Config& config) {
    if(disp)
        return true;

    logger->verbose("Creating display");

    w = config.getValue<int>("screen_width", "display");
    h = config.getValue<int>("screen_height", "display");
    disp = al_create_display(w, h);
    if(!disp) {
        w = 0;
        h = 0;
        return false;
    }

    ALLEGRO_MONITOR_INFO monitorInfo;
    if(!al_get_monitor_info(0, &monitorInfo))
        throw std::runtime_error("Unable to get monitor information");
    int monitor_width = monitorInfo.x2;
    int monitor_height = monitorInfo.y2;

    al_set_window_title(disp, "Minicraft");

    BitMap icon;
    icon.loadFile(Config::DATA_DIR/"icon.png");

    al_set_display_icon(disp, icon.bitmap);

    if(config.getValue<int>("anti-aliasing", "display")) {
        al_set_display_option(disp, ALLEGRO_SAMPLE_BUFFERS, 1);
        al_set_display_option(disp, ALLEGRO_SAMPLES, 8);
    }

    if(config.getValue<int>("fullscreen", "display"))
        al_set_display_flag(disp, ALLEGRO_FULLSCREEN_WINDOW, true);
    else
        al_set_window_position(disp,
                               monitor_width / 2 - w / 2,
                               monitor_height / 2 - h / 2);


    ALLEGRO_TRANSFORM t;
    al_identity_transform(&t);
    al_scale_transform(&t, PIXEL_SCALE_FACTOR, PIXEL_SCALE_FACTOR);
    al_use_transform(&t);

    logger->info("Display created: width=" + std::to_string(w) + " height=" + std::to_string(h));

    w = al_get_display_width(disp) / PIXEL_SCALE_FACTOR;
    h = al_get_display_height(disp) / PIXEL_SCALE_FACTOR;

    return true;
}

ALLEGRO_EVENT_SOURCE *Display::getEventSource() const {
    assert(disp != nullptr);
    return al_get_display_event_source(disp);
}

void Display::render() const {
    al_set_target_backbuffer(disp);
    al_flip_display();
}

void Display::clearScreen(const Colour &colour) {
    assert(disp != nullptr);
    al_set_target_backbuffer(disp);
    al_clear_to_color(colour);
}

std::size_t Display::width() const {
    return w;
}

std::size_t Display::height() const {
    return h;
}

Display::operator ALLEGRO_DISPLAY *() {
    return disp;
}
