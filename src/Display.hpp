/**
 * @file Display.hpp
 * @brief
 */
#ifndef MINICRAFT_DISPLAY_HPP
#define MINICRAFT_DISPLAY_HPP

#include <allegro5/allegro5.h>

#include <jonmiller131313/logging.hpp>

#include "graphics/Colour.hpp"
#include "Config.hpp"
#include "events/EventGenerator.hpp"

namespace minicraft {

class Display : public EventGenerator {
    public:
        static constexpr float PIXEL_SCALE_FACTOR = 3;

        Display();

        bool createDisplay(const Config &config);

        ~Display();

        ALLEGRO_EVENT_SOURCE *getEventSource() const override;

        void render() const;

        void clearScreen(const Colour &colour = Colour::BLACK);

        operator ALLEGRO_DISPLAY *();

        std::size_t width() const;

        std::size_t height() const;

    private:
        ALLEGRO_DISPLAY *disp;

        std::size_t w;
        std::size_t h;
};

};

#endif
